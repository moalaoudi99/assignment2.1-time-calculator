package Default;


public class CalculateTimeDifference {
    int timeDiff; // difference between the two times, in minutes
    public CalculateTimeDifference(int t1, int t2) {
        timeDiff = timeInMinutes(t2)-timeInMinutes(t1);
        if (timeDiff < 0)
            timeDiff = timeDiff + (60*24); // This handles the case where t2 is earlier than t1
    }
    // Given a military-format time, returns the number of minutes since midnight
    private int timeInMinutes(int t) {
        return (t/100)*60 + (t%100);
    }
    public int getHours() {
        return timeDiff / 60;
    }
    public int getMinutes() {
        return timeDiff %60;
    }
}